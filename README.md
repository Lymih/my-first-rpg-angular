# Projet Jeu porté sur angular

## Approche 
Pour cette nouvelle version, je me suis concentrée sur la partie affichage plutôt que sur l'algo pur. La version de ce rendu n'est pas terminée mais montre que j'ai trouvé le moyen de faire circuler les données entre les différents composants en utilisant les @Output().

## Affichage
J'ai construit le corps de ma page à l'intérieur du composant principal (app-root). La page est découpée en 3 parties : Header, Footer et le corps de la page qui affiche un composant selon une condition ***ngIf**.
Selon la valeur de la propriété **current** de app-root, le composant affiché sera différent. Par défault, cette propriété a pour valeur *"main_menu"*. Au chargement de la page, c'est donc le composant **app-main-menu** qui est affiché.
Le changement de la valeur *current* de app-root se fait par un système d'OutPut et l'appel d'une méthode *switchContent('...')* depuis le composant enfant de app-root.

Le rendu actuelle va jusqu'à l'affichage du composant *recap_player* qui affiche les caractéristiques du joueur qui vient d'être créé.

## Architecture et algo
J'ai créé un fichier *entities.ts* qui regroupe les principales interfaces de mon jeu. J'ai choisi de créer des interfaces plutôt que des classes car je n'arrivais pas à maitriser correctement l'instanciation.

J'ai commencé à créer l'algo du jeu dans des services : Le PlayerService contient les méthodes relatives à la gestion d'un *Player* et EnemyService contient celles qui sont relatives à la gestion d'un *Enemy*. 
Je n'ai pas pu tester toutes les méthodes. Pour l'instant, j'ai réussi à instancier un nouveau joueur et à afficher ses propriétés dans le composant *recap_player*. Il me reste également à travailler sur les autres entités (Game et Fight).

## Conclusion

L'algo de mon projet java étant à revoir avant de coder quelque chose de propre, je n'ai pas pu le retravailler dans sa totalité et présenter une 1ère version fonctionnelle de mon jeu pour ce rendu par manque de temps. Mais j'ai quand même réussi à réutiliser les notions que l'ont a vu en cours, en essayant au mieux d'intégrer les bonnes pratiques, telles que la logique MVC.




