import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  content = 'new_player';

  constructor() { }
  
  @Output() newCurrentEvent = new EventEmitter<string>();

  switchGameContent(content:string){
     this.newCurrentEvent.emit(content);
    }
  ngOnInit(): void {
  }

}
