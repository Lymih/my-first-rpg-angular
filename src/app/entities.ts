export interface Player {
  name: string;
  currentHp: number;
  maxHp: number;
  lvl: number;
  atk: number;
  def: number;

}

export interface Enemy {
  type: string;
  currentHp: number;
  maxHp: number;
  lvl: number;
  atk: number;
  def: number;
  
}
export interface Game {
  player:Player;
  reTry:number;
  potions:number;
}
export interface Fight {
  player:Player;
  enemy:Enemy;
}