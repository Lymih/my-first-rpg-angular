import { Injectable } from '@angular/core';
import { Player } from './entities';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
private player!:Player;

createPlayer(name: string){
  this.player = {
    name: name,
    currentHp: 100,
    maxHp: 100,
    lvl: 1,
    atk: 2,
    def: 2
  }
}
getPlayer():Player{
  return this.player!
}
lvlUpPlayer(){
  this.player.maxHp=this.player.maxHp+5;
  this.player.lvl=this.player.lvl+1;
  this.player.atk=this.player.atk+2;
  this.player.def=this.player.def+2;
}
resetPlayerHp(){
  this.player.currentHp=this.player.maxHp;
}
hitPlayer(dmg:number){
this.player.currentHp=this.player.currentHp-dmg;
}

healPlayer(hp:number){
  this.player.currentHp=this.player.currentHp+hp
  if (this.player.currentHp > this.player.maxHp){
    this.calculateRecoveredHpPlayer(hp,this.player.currentHp-this.player.maxHp);
  }
}
calculateRecoveredHpPlayer(hp:number,overHeal:number){
  return hp-overHeal;
}

  constructor() { }
}
