import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

 @Output() newCurrentEvent = new EventEmitter<string>();

  switchContent(value:string){
    this.newCurrentEvent.emit(value);
  }
  constructor() { }
  
  ngOnInit(): void {
  }

}
