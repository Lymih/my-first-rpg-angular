import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Player } from '../entities';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-new-player',
  templateUrl: './new-player.component.html',
  styleUrls: ['./new-player.component.css']
})
export class NewPlayerComponent implements OnInit {
playerName!:string;
constructor(private pS:PlayerService){
}
  submit(form: NgForm) {
    if (this.checkInput(form.value.player_name)) {
      this.playerName=form.value.player_name;
      this.pS.createPlayer(this.playerName);
      this.switchContent('player_recap');
    } else {
      console.log('error');
    }

  }

  @Output() newCurrentEvent = new EventEmitter<string>();



  switchContent(value: string) {
    this.newCurrentEvent.emit(value);
  }

  checkInput(playerName: string): boolean {
    if (playerName.length < 3 || playerName.length > 12) {
      return false;
    }
    else {
      return true;
    }
  }

  ngOnInit(): void {
  }

}
