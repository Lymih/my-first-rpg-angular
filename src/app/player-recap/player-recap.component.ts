import { Component, OnInit } from '@angular/core';
import { Player } from '../entities';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-player-recap',
  templateUrl: './player-recap.component.html',
  styleUrls: ['./player-recap.component.css']
})
export class PlayerRecapComponent implements OnInit {
player:Player=this.pS.getPlayer();
  constructor(private pS:PlayerService) { }

  ngOnInit(): void {
    this.pS.getPlayer();
  }

}
