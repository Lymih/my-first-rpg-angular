import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerRecapComponent } from './player-recap.component';

describe('PlayerRecapComponent', () => {
  let component: PlayerRecapComponent;
  let fixture: ComponentFixture<PlayerRecapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerRecapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerRecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
