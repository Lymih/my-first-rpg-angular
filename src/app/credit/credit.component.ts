import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {

 @Output() newCurrentEvent =new EventEmitter<string>();
  constructor() { }

switchContent(value:string){
  this.newCurrentEvent.emit(value);
}

  ngOnInit(): void {
  }

}
