import { Injectable } from '@angular/core';
import { Enemy } from './entities';
import { PlayerService } from './player.service';

@Injectable({
  providedIn: 'root'
})
export class EnemyService {
  enemy!: Enemy;

  getEnemy() {
    return this.enemy;
  }
  rollEnemyType(): string {
    let types: string[] = ['Wolf', 'Bear', 'Bandit', 'Lizard']
    let rolledType = types[Math.random() * ((types.length + 1) - 0) + 0]
    console.log(rolledType);
    return rolledType;
  }

  createEnemy() {
    this.enemy = {
      type: this.rollEnemyType(),
      currentHp: this.pS.getPlayer().currentHp,
      maxHp: this.pS.getPlayer().maxHp,
      lvl: this.pS.getPlayer().lvl,
      atk: this.pS.getPlayer().atk,
      def: this.pS.getPlayer().def
    }
  }

  constructor(private pS:PlayerService) { }
}
