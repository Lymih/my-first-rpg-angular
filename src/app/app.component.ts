import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-rpg-angular';
  current='main_menu';

  switchContent(current:string){
    this.current=current;
  }

}

